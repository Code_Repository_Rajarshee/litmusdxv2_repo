/**
 * 
 */
package com.litmusdxv2.test;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.sikuli.script.FindFailed;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import com.litmusdxv2.pages.LoginPage;
import com.litmusdxv2.utility.BrowserFactory;
import com.litmusdxv2.utility.WaitElement;

import org.junit.Assert;

/**
 * @author Rajarshee
 *
 */

public class LoginRegressionSuite {

	WebDriver driver;
	//BrowserFactory browserFactory;
	//LoginPage loginPage;
	WaitElement wait;
	LoginPage loginPage;


	
	@BeforeMethod
	public void appLaunch() throws FindFailed, InterruptedException {

		//This will Launch browser and specific url
		driver = BrowserFactory.launchBrowser("chrome","https://www.hellolyf.com/aftab.jsp");

		//Creating Page Object using page factory
		//loginPage = PageFactory.initElements(driver, LoginPage.class);

		//Call the Method
		//loginPage.clickLoginbtn();
		//loginPage.loginDone("9748632179","123456");
		//loginPage.clkloginsecnd();
		//login_page.clickwhatsup();
		//login_page.clickljnk1();
		//login_page.clickljnk2();
		//login_page.clickljnk3();
		//login_page.clickljnk4();
		//login_page.clickljnk5();
		//login_page.clickljnk6();
		//login_page.clickljnk7();
		//login_page.signuplink();
		//login_page.backToLoginPage();
		//login_page.clickljnk8();
		//login_page.clickljnk9();
		//login_page.clickljnk10();
		//login_page.clickhome();
		//login_page.clicklinkdin();
		//login_page.clicktwitter();
		//login_page.clickfacebook();
		//login_page.clickLanguage();
		//login_page.clickenglishLanguage();
		//login_page.clickfrenchLanguage();

	}


	@Test(priority = 0 , enabled = true, description="TC_001: Verifying valid login functionality")
	public void verifyValidLogin() {
		try {
			loginPage = PageFactory.initElements(driver, LoginPage.class);
			loginPage.clickLoginbtn();// Clicking on Login Button
			loginPage.loginDone("9748632179","123456");// Entering valid Password.
			loginPage.clkloginsecnd();// Again clicking on Login Button.
			//loginPage.clickalertButton(); // Clicking on Alert Button.
			Assert.assertEquals(true, loginPage.verifyValidLogin()); // Verifying valid login scenario.
		}
		catch(Exception e)
		{
			System.out.println(e);
		}

	}


	@Test(priority = 1, enabled = true, description="TC_002: Verifying invalid login functionality")
	public void verifyInvalidLogin() {
		try 
		{
			loginPage = PageFactory.initElements(driver, LoginPage.class);
			loginPage.clickLoginbtn(); // Clicking on Login Button
			loginPage.loginDone("97486321790","1234567"); // Entering valid Password.
			loginPage.clkloginsecnd(); // Again clicking on Login Button.
			//loginPage.clickalertButton(); // Clicking on Alert Button.
			Assert.assertEquals(true, loginPage.verifyInValidLogin()); // Verifying valid login scenario.
		}
		catch(Exception e)
		{
			System.out.println(e);
		}

	}

	@AfterMethod
	public void closeBrowser() 
	{
		WaitElement.waitTill(5000);
		//driver.close();
		driver.quit();
	}


}
