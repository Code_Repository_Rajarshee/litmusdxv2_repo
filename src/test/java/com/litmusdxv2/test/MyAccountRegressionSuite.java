/**
 * 
 */
package com.litmusdxv2.test;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.litmusdxv2.pages.LoginPage;
import com.litmusdxv2.pages.MyAccount;
import com.litmusdxv2.utility.BrowserFactory;
import com.litmusdxv2.utility.WaitElement;

/**
 * @author Rajarshee
 *
 */
public class MyAccountRegressionSuite 
{

	WebDriver driver;
	//WaitElement wait;
	LoginPage loginPage;
	MyAccount myAcoount = new MyAccount(driver);
	//MyAccountRegressionSuite myAccountRegSuite;

	@BeforeMethod
	public void appLaunch()
	{
		driver = BrowserFactory.launchBrowser("chrome", "https://www.hellolyf.com/aftab.jsp");

	}

	@Test(priority = 0 , enabled = true, description="TC_001: Verifying Entering in History section or Not")
	public void chkHistory(){
		try {
			loginPage = PageFactory.initElements(driver, LoginPage.class);
			loginPage.clickLoginbtn();// Clicking on Login Button
			loginPage.loginDone("9748632179","123456");// Entering valid Password.
			loginPage.clkloginsecnd();// Again clicking on Login Button.
			myAcoount = loginPage.clickaccount();// Click on My Account tab.
			myAcoount.clkappintmnt();// Click on Appointment link
			Assert.assertEquals(true, myAcoount.clkhistry()); // Verifying Check history clicked properly or not.
		}catch(Exception e)
		{
			System.out.println(e);
		}
	}

	@AfterMethod
	public void closeBrowser() 
	{
		WaitElement.waitTill(5000);
		//driver.close();
		driver.quit();
	}


}
