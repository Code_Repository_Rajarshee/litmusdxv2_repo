/**
 * 
 */
package com.litmusdxv2.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;

import com.litmusdxv2.utility.WaitElement;

/**
 * @author Rajarshee
 *
 */
public class MyAccount 
{

	WebDriver driver;
	WaitElement wait;

	@FindBy(xpath="//a[@id='showAppointmentsList']") 
	WebElement shwappointmnt;

	public MyAccount(WebDriver driver)
	{
		this.driver = driver;
	}

	public boolean clkhistry()
	{
        String expURL = "https://hellolyf.com/getAppointmentList";
        String actURL = driver.getCurrentUrl();
        if(expURL.equalsIgnoreCase(actURL))
        {
        	return true;
        } else 
        {
        	return false;
        }
		
	}
	
	public void clkappintmnt() throws FindFailed {
		WaitElement.waitTill(5000);
		Screen screen = new Screen();
		Pattern object3 = new Pattern(".\\Project_Image\\Appointment_Tab.PNG");
		screen.click(object3);
		WaitElement.waitTill(10000);
	}


}
