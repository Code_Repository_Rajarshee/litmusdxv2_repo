/**
 * 
 */
package com.litmusdxv2.pages;

import java.util.Set;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;

import com.litmusdxv2.utility.WaitElement;

/**
 * @author Rajarshee
 * This Page Contains only Login Page Details
 *
 */

public class LoginPage 
{

	WebDriver driver;
	WaitElement Wait;

	@FindBy(linkText="About Us")
	WebElement abtus;

	@FindBy(linkText="Terms & Conditions")
	WebElement TandC;

	@FindBy(linkText="Privacy policy")
	WebElement PP;

	@FindBy(linkText="System Requirements")
	WebElement SR;

	@FindBy(linkText="Refund policy")
	WebElement RP;

	@FindBy(partialLinkText="FAQ")
	WebElement faq;

	@FindBy(linkText="www.facebook.com/hellolyf")
	WebElement fblink;

	@FindBy(partialLinkText="DOCTOR & ADMIN SECTION")
	WebElement docandadmin;

	@FindBy(xpath="//a[contains(text(),'Dr. Login')]")
	WebElement drlogin;

	@FindBy(xpath="//a[contains(text(),'Admin Login')]")
	WebElement adminlogin;

	@FindBy(linkText="Sign up here?")
	WebElement signuplink;

	//indBy(linkText="About Us")
	//WebElement;

	//@FindBy(id="home-icon")
	//WebElement Homelink;

	@FindBy(id="loginMobileNo")
	WebElement UserNo;

	@FindBy(id="password")
	WebElement passw;

	@FindBy(linkText="FAQ")
	WebElement linktxt;

	@FindBy(linkText="Back to login")
	WebElement backtologin;

	//@FindBy(linkText=" My Account")
	//WebElement myaccont;

	//@FindBy(name="userName")
	//WebElement username;

	//@FindBy(name="password")
	//WebElement passWord;

	//@FindBy(name="submit")
	//WebElement Submit;

	//@FindBy(xpath="//li[2]//input[1]")
	//WebElement txtBoxUserName;

	//@FindBy(xpath="//li[4]//input[1]")
	//WebElement txtBoxPassword;

	//@FindBy(xpath="//button[@class='btn btn-md btn-success login-button']")
	//WebElement btnLogin; 

	//@FindBy(xpath="//button[@class='btn btn-md btn-success']")
	//WebElement alert;

	public LoginPage(WebDriver driver)
	{
		this.driver = driver;
	}

	/*public void userNameEntry(String userName)
	{
		WaitElement.waitForElement(txtBoxUserName, 50, driver);
		txtBoxUserName.sendKeys(userName);
		WaitElement.waitTill(5000);
	}

	public void passwordEntry(String password)
	{
		WaitElement.waitForElement(txtBoxPassword, 50, driver);
		txtBoxPassword.sendKeys(password);
		WaitElement.waitTill(5000);
	}


	public void clickLoginButton()
	{
		WaitElement.waitForElement(btnLogin, 50, driver);
		btnLogin.click();
		WaitElement.waitTill(5000);
	}

	public DashBoardPage clickalertButton()
	{
		WaitElement.waitForElement(alert, 50, driver);
		alert.click();
		WaitElement.waitTill(5000);
		return PageFactory.initElements(driver, DashBoardPage.class);
	}

	public boolean verifyValidLogin() {
		WaitElement.waitTill(5000);
		String expectedURL = "https://medikate.org/litmusDXV2/dashboard";
		String actualURL = driver.getCurrentUrl();
		if(expectedURL.equals(actualURL)) {
			return true;
		}else{
			return false;
		}
	}
	public boolean verifyInvalidLogin() {
		WaitElement.waitTill(5000);
		String expectedURL = "https://medikate.org/litmusDXV2/login";
		String actualURL = driver.getCurrentUrl();
		if(expectedURL.equals(actualURL)) {
			return true;
		}else{
			return false;
		}
	}*/

	/*public void clickljnk1() throws FindFailed
	{
		Screen screen = new Screen();
		WaitElement.waitTill(5000);
		abtus.click();
		WaitElement.waitTill(5000);
		Pattern object1 = new Pattern(".\\Project_Image\\Homelink2.PNG");
		screen.click(object1);
		WaitElement.waitTill(5000);
	}

	public void clickljnk2() throws FindFailed
	{
		Screen screen = new Screen();
		WaitElement.waitTill(5000);
		TandC.click();
		WaitElement.waitTill(5000);
		Pattern object1 = new Pattern(".\\Project_Image\\Homelink2.PNG");
		screen.click(object1);
		WaitElement.waitTill(5000);
	}

	public void clickljnk3() throws FindFailed
	{
		Screen screen = new Screen();
		WaitElement.waitTill(5000);
		PP.click();
		WaitElement.waitTill(5000);
		Pattern object1 = new Pattern(".\\Project_Image\\Homelink2.PNG");
		screen.click(object1);
		WaitElement.waitTill(5000);
	}

	public void clickljnk4() throws FindFailed
	{
		Screen screen = new Screen();
		WaitElement.waitTill(5000);
		SR.click();
		WaitElement.waitTill(5000);
		Pattern object1 = new Pattern(".\\Project_Image\\Homelink2.PNG");
		screen.click(object1);
		WaitElement.waitTill(5000);
	}

	public void clickljnk5() throws FindFailed
	{
		Screen screen = new Screen();
		WaitElement.waitTill(5000);
		RP.click();
		WaitElement.waitTill(5000);
		Pattern object1 = new Pattern(".\\Project_Image\\Homelink2.PNG");
		screen.click(object1);
		WaitElement.waitTill(5000);
	}

	public void clickljnk6() throws FindFailed
	{
		Screen screen = new Screen();
		WaitElement.waitTill(5000);
		faq.click();
		WaitElement.waitTill(5000);
		Pattern object1 = new Pattern(".\\Project_Image\\Homelink2.PNG");
		screen.click(object1);
		WaitElement.waitTill(5000);
	}

	public void clickljnk7() throws FindFailed
	{
		//Screen screen = new Screen();
		WaitElement.waitTill(5000);
		fblink.click();
		WaitElement.waitTill(5000);
		//Pattern object1 = new Pattern(".\\Project_Image\\Homelink2.PNG");
		//screen.click(object1);
		driver.navigate().back();
		WaitElement.waitTill(5000);
	}

	public void clickljnk8() throws FindFailed
	{
		String parent = driver.getWindowHandle();
		//Screen screen = new Screen();
		WaitElement.waitTill(5000);
		docandadmin.click();
		WaitElement.waitTill(5000);
		//Pattern object1 = new Pattern(".\\Project_Image\\Homelink2.PNG");
		//screen.click(object1);
		drlogin.click();
		WaitElement.waitTill(5000);
		driver.switchTo().window(parent);
		WaitElement.waitTill(5000);
		adminlogin.click();
		WaitElement.waitTill(5000);
		driver.switchTo().window(parent);
		WaitElement.waitTill(5000);
	}

	/*public void clickljnk9() throws FindFailed
	{
		//Screen screen = new Screen();
		WaitElement.waitTill(5000);
		drlogin.click();
		WaitElement.waitTill(5000);
		//Pattern object1 = new Pattern(".\\Project_Image\\Homelink2.PNG");
		//screen.click(object1);
		driver.navigate().back();
		WaitElement.waitTill(5000);
	}

	public void clickljnk10() throws FindFailed
	{
		//Screen screen = new Screen();
		WaitElement.waitTill(5000);
		adminlogin.click();
		WaitElement.waitTill(5000);
		//Pattern object1 = new Pattern(".\\Project_Image\\Homelink2.PNG");
		//screen.click(object1);
		driver.navigate().back();
		WaitElement.waitTill(5000);
	}*/

	/*public void clickhome() throws FindFailed 
	{
		Screen screen = new Screen();
		Pattern object1 = new Pattern(".\\Project_Image\\Home_Icon.PNG");
		screen.click(object1);
		WaitElement.waitTill(5000);
	}

	public void clickLanguage() throws FindFailed 
	{
		Screen screen = new Screen();
		Pattern object1 = new Pattern(".\\Project_Image\\Language.PNG");
		screen.click(object1);
		WaitElement.waitTill(5000);
		Pattern object3 = new Pattern(".\\Project_Image\\French.PNG");
		screen.click(object3);
		WaitElement.waitTill(5000);
		//JavascriptExecutor js = (JavascriptExecutor) driver;
		//js.executeScript("arguments[0].scrollIntoView();", linktxt);
		//WaitElement.waitTill(5000);	
	}


	public void clickfrenchLanguage() throws FindFailed 
	{
		Screen screen = new Screen();
		Pattern object2 = new Pattern(".\\Project_Image\\Fench2.PNG");
		screen.click(object2);
		WaitElement.waitTill(5000);
		Pattern object3 = new Pattern(".\\Project_Image\\English.PNG");
		screen.click(object3);
		WaitElement.waitTill(5000);

	}

	public void clicktwitter() throws FindFailed 
	{
		Screen screen = new Screen();
		Pattern object1 = new Pattern(".\\Project_Image\\twitter.PNG");
		screen.click(object1);
		//for(String child : driver.getWindowHandles()){
			//{
			//	driver.switchTo().window(child);
			//	driver.close();
			//}
	//	}
		WaitElement.waitTill(5000);
		String parent = driver.getWindowHandle();
		driver.switchTo().window(parent);
	}

	public void clickfacebook() throws FindFailed 
	{
		Screen screen = new Screen();
		Pattern object1 = new Pattern(".\\Project_Image\\facebook.PNG");
		screen.click(object1);
		//for(String child : driver.getWindowHandles()){
			//{
			//	driver.switchTo().window(child);
			//	driver.close();
			//}
	//	}
		WaitElement.waitTill(5000);
		String parent = driver.getWindowHandle();
		driver.switchTo().window(parent);
	}

	public void clicklinkdin() throws FindFailed 
	{
		Screen screen = new Screen();
		Pattern object1 = new Pattern(".\\Project_Image\\linkdin.PNG");
		screen.click(object1);
		//for(String child : driver.getWindowHandles()){
			//{
			//	driver.switchTo().window(child);
			//	driver.close();
			//}
	//	}
		WaitElement.waitTill(5000);
		String parent = driver.getWindowHandle();
		driver.switchTo().window(parent);
	}

	public void clickwhatsup() throws FindFailed 
	{
		Screen screen = new Screen();
		Pattern object1 = new Pattern(".\\Project_Image\\whatsup.PNG");
		screen.click(object1);
		WaitElement.waitTill(2500);
		System.out.println(driver.switchTo().alert().getText());
		driver.switchTo().alert().dismiss();
		//for(String child : driver.getWindowHandles()){
			//{
			//	driver.switchTo().window(child);
			//	driver.close();
			//}
	//	}
		WaitElement.waitTill(5000);
		String parent = driver.getWindowHandle();
		driver.switchTo().window(parent);
	}*/

	public void clickLoginbtn() throws FindFailed 
	{
		WaitElement.waitTill(500);
		Screen screen = new Screen();
		Pattern object1 = new Pattern(".\\Project_Image\\Login-Image.PNG");
		screen.click(object1);
		WaitElement.waitTill(500);
	}

	public void loginDone(String uname, String pass) 
	{
		WaitElement.waitTill(500);
		UserNo.sendKeys(uname);
		passw.sendKeys(pass);
		WaitElement.waitTill(500);
	}

	public void clkloginsecnd() throws FindFailed 
	{
		Screen screen = new Screen();
		Pattern object2 = new Pattern(".\\Project_Image\\Second-Login.PNG");
		screen.click(object2);
		WaitElement.waitTill(500);
	}

	/*public void signuplink()
	{
		WaitElement.waitTill(5000);
		signuplink.click();
		WaitElement.waitTill(5000);
	}

	public void backToLoginPage()
	{
		WaitElement.waitTill(5000);
		backtologin.click();
		WaitElement.waitTill(5000);
	}*/

	public MyAccount clickaccount() throws InterruptedException, FindFailed
	{
		WaitElement.waitTill(500);
		Screen screen = new Screen();
		Pattern object2 = new Pattern(".\\Project_Image\\MyAccount_Image.PNG");
		screen.click(object2);
		WaitElement.waitTill(500);
		return PageFactory.initElements(driver, MyAccount.class);
	}

	/* Verifying valid login */
	public boolean verifyValidLogin()
	{		
		String expURL = "https://hellolyf.com/loginvc.do";
		String actURL = driver.getCurrentUrl();

		if(expURL.equals(actURL)) {
			return false;
		}else {
			return true;
		}

	}

	/* Verifying invalid login */
	public boolean verifyInValidLogin()
	{
		String expURL = "https://hellolyf.com/aftab.jsp";
		String actURL = driver.getCurrentUrl();

		if(expURL!=(actURL)) {
			return true;
		}else {
			return false;
		}
	}
}
